##Purpose: WVU CS481 - Capstone Senior Design Project##
Author: Chris Gatto
Author: Hirsh Parikshak

This project deals Computer Vision based surveillance, facial re-identification, dynamic training of new individuals, and multi-threaded video capture and processing.

Learn more at http://cjgatto.bitbucket.org/